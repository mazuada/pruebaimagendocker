const express = require ('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require ('body-parser');



app.use(bodyParser.json());
app.listen(port);
console.log('API escuchando puerto' + port);

app.get('/api/pruebas/hello',
  function(req,res){
    console.log('Hello desde Api pruebas molona');
    res.send({"msg":"Hello desde Api pruebas molona"});
  }
);

app.get('/api/pruebas/usuarios',
  function(req,res){
    console.log('GET api/pruebas/usuarios');
    var result ={};
    var usuarios = require ('./usuarios.json');

    result.cont = usuarios.length;
    result.usuarios = usuarios;
    console.log('numero de usuarios' + result.cont);
    res.send(result.usuarios);
  }
);

app.post ('/api/pruebas/usuario',
  function(req,res){
    console.log('POST api/pruebas/usuario');
    var nuevoUsuario = {
      "id": req.body.id,
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": req.body.password
    };
    var usuarios = require ('./usuarios.json');
    usuarios.push(nuevoUsuario);
    WriteUserDatatoFile(usuarios);

    res.send('{"msg":"usuario añadido con éxito"}')
  }
);

app.delete('/api/pruebas/usuario/:id',
  function(req,res){
    console.log('DELETE api/pruebas/usuario:' + req.params.id);
    var usuarios = require('./usuarios.json');
    var cont = 0;
    for (user of usuarios){
      cont++;
      if(user.id == req.params.id){
        console.log('usuario con posición:' + cont);
          usuarios.splice(cont-1,1);
          WriteUserDatatoFile(usuarios);
          res.send('{"msg":"usuario borrado con éxito"}');
          break;
      }
    }
  }
);

app.post ('/api/pruebas/login',
function(req,res){
  console.log('POST api/pruebas/login');
  var usuarios = require('./usuarios.json');
  var resultado= {};
  resultado.mensaje="usuario logado incorrectamente";
  var cont = 0;

  for (user of usuarios) {
    cont ++;
    if (user.email == req.body.email && user.password == req.body.password){
        user.logged = true;
        resultado.mensaje = "usuario logado correctamente";
        resultado.id = user.id;
        usuarios.splice(cont-1,1,user);
        WriteUserDatatoFile(usuarios);
        break;
      }
    }
  console.log(resultado.mensaje);
  res.send(resultado);
  }
);

app.post ('/api/pruebas/logout',
function(req,res){
  console.log('POST api/pruebas/logout');
  var usuarios = require('./usuarios.json');
  var resultado= {};
  resultado.mensaje="logout incorrecto";
  var cont = 0;

  for (user of usuarios) {
    cont ++;
    if (user.id == req.body.id && user.logged == true){
        resultado.mensaje = "logout correcto";
        resultado.id = user.id;
        delete user.logged;
        usuarios.splice(cont-1,1,user);
        WriteUserDatatoFile(usuarios);
        break;
      }
    }
  console.log(resultado.mensaje);
  res.send(resultado);
  }
);

function WriteUserDatatoFile(data) {
  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json",jsonUserData, "utf-8",
    function(err) {
      if (err){
        console.log(err);
      } else {
        console.log("Datos escritos en fichero");
      }
    }
)
}
