# Imagen origen
FROM node:6.0.0

#Carpeta raiz
WORKDIR /apitechuprueba

# Copia de archivos de la carpeta local a apitechuprueba
ADD . /apitechuprueba

#Si en .dockerignore añadimos ./node_modules
#hay que poner aqui RUN install npm

# Exponer puerto
EXPOSE 3000

#Comando de inicialización
CMD ["npm", "start"]
